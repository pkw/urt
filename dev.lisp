(uiop:define-package :urt/dev
  (:mix :cl 
        :alexandria 
        :serapeum 
        :bordeaux-threads)
  (:export #:mvb 
           #:t1
           #:thr
           #:thr-10))

(in-package :urt/dev)

(defun vals ()
  (values 1 2 3 4 5 6 7 8))

(defun mvb () 
  (format t "multiple-value-bind test...~%")
  (multiple-value-bind (v1 v2 v3) (vals)
    (format t "v1:~a v2:~a v3:~a~%" v1 v2 v3)))
  
(defun t1 ()
  (alexandria:iota 11))

(defun thr ()
  (format t "thr...put it in a thread~%")
  (let ((th (make-thread #'sleeper)))
    (format t "th:~a~%" th)
    (sleep 2)
    (bt:destroy-thread th)))
    
(defun thr-10 (timeout)
  (format t "thr-10...~%")
  (let ((thread (bt:make-thread #'sleeper
                  :name "maybe-costly-thread")))
    (handler-case
      (bt:with-timeout (timeout) 
        (print "pre-join")
        (bt:join-thread thread) 
        (print "post-join"))
      (bt:timeout ()            
        (print "destroy")
        (bt:destroy-thread thread)))))

(defun sleeper ()
  (dolist (i '(1 2 3 4 5 6 7))
    (format t "i:~a~%" i)
    (sleep 1)))
    

