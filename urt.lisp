(uiop:define-package :urt/urt
  (:mix :cl 
        :str
        :local-time 
        :usocket 
        :babel 
        :split-sequence 
        :iterate
        :bordeaux-threads ; provides bt:*
        :alexandria)
  (:export #:*main-server*
           #:*timeout*
           #:get-raw-response
           #:get-cached-raw-response 
           #:*raw-resp-cache-time* 
           #:*raw-resp-cache* 
           #:parse-servers
           #:format-server
           #:get-raw-server-status 
           #:get-raw-server-status-timeout
           #:server-vars
           #:num-players))

(in-package :urt/urt)

(defparameter *main-server* '("master.urbanterror.info" . 27900))
(defparameter *timeout* 2)
(defparameter *raw-resp-cache-time* nil)
(defparameter *raw-resp-cache* nil)

(defparameter +prefix+ 
  (make-array 4 
              :element-type '(unsigned-byte 8)
              :initial-contents '(#xff #xff #xff #xff)))

(defun send (socket string)
  (let* ((buffer (babel:string-to-octets string :encoding :ascii))
         (buffer (concatenate '(vector (unsigned-byte 8)) +prefix+ buffer)))
    ;;(format t "send:~a~%" buffer)
    (usocket:socket-send socket buffer (length buffer))))

;; timeout needed ? 
;; socket-receive returns 4 values: 
;;   return-buffer return-length remote-host remove-port
;; 92 is slash which starts each section of ip address ...
(defun get-raw-response ()
  (let ((socket (usocket:socket-connect (car *main-server*) (cdr *main-server*)
                 :protocol :datagram
                 :timeout 3)))
    (send socket "getservers 68 empty full")
    ;;(multiple-value-bind (buffer size client receive-port)
    (multiple-value-bind (buffer size)
      (usocket:socket-receive socket nil 65507)
      ;;(format t "size:~a client:~a receive-port:~a~%" size client receive-port)
      (subseq buffer 0 size))))

;; take out the caching and do that in the urt-web code. (using the db)
(defun get-cached-raw-response ()
  (let ((do-rr nil))
    (if (null *raw-resp-cache-time*) 
      (setf do-rr t)
      (progn
        (format t "compare the time to 2 minutes from now~%")
        (let ((2min (local-time:adjust-timestamp *raw-resp-cache-time* (offset :minute 2)))
              (now (local-time:now)))
          (if (local-time:timestamp> now 2min)
            (setf do-rr t)))))
    (format t "do-rr:~a~%" do-rr)
    (if do-rr
      (let ((rr (get-raw-response)))
        (setf *raw-resp-cache-time* (local-time:now))
        (setf *raw-resp-cache* rr))))
  *raw-resp-cache*) 

(defun format-server (s)  
  (format nil "~a.~a.~a.~a:~a"
    (nth 1 s) 
    (nth 2 s) 
    (nth 3 s) 
    (nth 4 s)
    (+ (ash (nth 5 s) 8)
       (nth 6 s))))
  
(defun parse-servers (raw-resp)
  "(92 151 80 46 84 109 57)"
  (let* ((first-section t)
         (s (iter (for i in-sequence raw-resp)
                  (for x from 0)
                  (if first-section ;; skip the first section
                    ;; i can use the else clause instead of this progn
                    (progn 
                      (if (= i 92)
                        (progn
                          (setf first-section nil)
                          (setf x 0)))
                      (if (/= i 92)
                        (next-iteration))))
                  (if (= x 7)
                    (progn
                      (collect (format-server items) into itemss)
                      ;;(collect items into itemss)
                      (setf items '())
                      (setf x 0)))
                  (collect i into items)
                  (finally (return itemss)))))
    s))

(defun get-raw-server-status (ip port)
  (format t "get-raw-status ip:~a port:~a~%" ip port)
  (handler-case 
    (let ((socket (usocket:socket-connect ip port
                   :protocol :datagram
                   :timeout 3)))
      (send socket "getstatus")
      (multiple-value-bind (buffer size)
        (usocket:socket-receive socket nil 65507)
        (subseq buffer 0 size)))
    (error (e)
      (format t "error e:~a~%" e))))

#|
sometimes it just hangs and sometimes this happens:
USOCKET:CONNECTION-REFUSED-ERROR
USOCKET:HOST-UNREACHABLE-ERROR
either way the timeout catches it, but if I caught that error i 
wouldn't have to wait for the timeout in that case?
not worth it ?
|#
(defun get-raw-server-status-timeout (ip port)
  "Wrap get-raw-server-status with a bordeaux-threads timeout.
  Because it can hang forever."
  (let ((thr-1 (bt:make-thread (lambda () (get-raw-server-status ip port))
                 :name "get-raw-server-status-timeout")))
    (handler-case
      (bt:with-timeout (*timeout*) 
        (bt:join-thread thr-1))
      (bt:timeout ()            
        (format t "now destroying thread after timeout ip:~a port:~a~%" ip port)
        (bt:destroy-thread thr-1)))))

(defun server-vars (raw-server-status)
  (let* ((s (babel:octets-to-string (subseq raw-server-status 4 nil)))
         (lines (str:lines s))
         (raw-vars (nth 1 lines))
         (vars-list (str:split "\\" raw-vars :omit-nulls t))
         (l '()))
    (dolist (v vars-list)
      (push v l))
    (alexandria:plist-hash-table (reverse l) :test 'equalp)))

(defun num-players (raw-server-status)
  (let* ((s (babel:octets-to-string (subseq raw-server-status 4 nil)))
         (lines (str:lines s)))
    (pop lines)
    (pop lines)
    (length lines)))

