(defsystem "urt"
  :class :package-inferred-system
  ;;:serial t
  :depends-on (:babel
               :str
               :usocket
               :urt/all 
               :local-time))

(register-system-packages "urt/all" '(:urt))

