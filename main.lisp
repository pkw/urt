(uiop:define-package :urt/main
  (:mix :cl)
  (:export #:main))

(in-package :urt/main)

(defun help-menu () 
  (format t "[urt]~%")
  (format t "  1: init db~%")
  (format t "  2: pop-servers~%")
  (format t "  3: add server details (limit 10)~%"))

(defun choose-cmd (cmd &optional arg1)
  (format t "[urt*]~%")
  (case cmd
    (1 (format t "1"))
    (2 (format t "2"))
    (3 (format t "3"))))

(defun main (&optional arg1 arg2) 
  (if arg1
    (choose-cmd arg1 arg2)
    (help-menu)))

