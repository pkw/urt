PROJ=	${.CURDIR:T}
CLROOT=	$(HOME)/common-lisp

.PHONY: link

link:
	rm -rf $(CLROOT)/${PROJ}
	ln -s $(.CURDIR) $(CLROOT)/$(PROJ) 



